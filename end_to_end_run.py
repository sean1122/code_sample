from __future__ import division

import os
import random

import pandas as pd
import numpy as np
from tqdm import tqdm

import keras
from keras.layers import Input, Dense, Dropout, Conv1D, MaxPooling1D, UpSampling1D
from keras.models import Model, Sequential, load_model
import matplotlib.pyplot as plt

from TeamModel import TeamModel
from MatchModel import MatchModel
from data_loader import*

data_path = os.path.abspath('./Data')

def train_team_networks():

    team_networks = {}
    for team in tqdm(os.listdir(os.path.join(data_path, 'Teams'))):

        if os.path.exists(os.path.join(data_path,'Teams',team,'{}.h5'.format(team))):
            team_networks[team] = load_model(os.path.join(data_path,'Teams',
                                                            team,'{}.h5'.format(team)))
        else:
        
            # laod the team data
            team_data = np.array(get_team_data(team, columns, test=False))
            team_data = np.nan_to_num(team_data / team_data.max(axis=0))
            # make it 32 cols
            team_data = np.append(team_data, np.zeros((team_data.shape[0], 1)), axis=1)

            test_data = np.array(get_team_data(team, columns, test=True))
            test_data = np.nan_to_num(test_data / test_data.max(axis=0))
            test_data = np.append(test_data, np.zeros((test_data.shape[0], 1)), axis=1)

            # organize the data for training
            x_train = np.expand_dims(team_data[:120], axis=-1)
            x_valid = np.expand_dims(team_data[120:], axis=-1)
            x_test = np.expand_dims(test_data, axis=-1)

            input_shape = (team_data.shape[-1],1)
            params = {'timesteps': 5,
                      'latent_dim': 64,
                      'batch_size': 32,
                      'activation': 'tanh',
                      'optimizer': 'adagrad',
                      'loss': 'mean_squared_error',
                      'n_epoch': 200,
                      'verbose': False}

            team_networks[team] = TeamModel(team, input_shape, params)
            hist = team_networks[team].train(x_train, x_train, params, x_valid, x_valid)

            team_networks[team].eval(x_train, x_train, x_valid, x_valid, x_test, x_test)

            team_networks[team].predict(x_valid)

            team_networks[team].save(os.path.join(data_path,'Teams',team,'{}.h5'.format(team)))

    return team_networks

def train_match_predictor(team_networks):

    match_data, match_scores = get_match_data(team_networks, columns)
    match_test, match_test_scores = get_match_data(team_networks, columns, True)

    x_train, y_train = match_data[:1500,:], match_scores[:1500,:]
    x_valid, y_valid = match_data[1500:,:], match_scores[1500:,:]
    x_test, y_test = match_test, match_test_scores

    mode = 2
    y_train_ = categorize_labels(y_train, mode=mode)
    y_valid_ = categorize_labels(y_valid, mode=mode)
    y_test_ = categorize_labels(y_test, mode=mode)

    x_train, y_train_ = balance_train_data(x_train, y_train_)

    y_train_ = np.eye(len(set(y_train_)))[y_train_]
    y_valid_ = np.eye(len(set(y_valid_)))[y_valid_]
    y_test_ = np.eye(len(set(y_test_)))[y_test_]

    input_shape = (match_data.shape[1],)
    params = {'latent_dim': 500,
              'batch_size': 32,
              'activation': 'elu',
              'dropout': 0.5,
              'optimizer': 'adadelta',
              'loss': 'categorical_crossentropy',
              'n_epoch': 200,
              'verbose': False}

    match_predictor = MatchModel(input_shape, params, mode)
    hist = match_predictor.train(x_train, y_train_, params, x_valid, y_valid_)

    match_predictor.eval(x_train, y_train_, x_valid, y_valid_, x_test, y_test_)

    match_predictor.predict(x_test, y_test)

    return match_predictor

if __name__ == '__main__':

    team_networks = train_team_networks()

    match_predictor = train_match_predictor(team_networks)

    match_predictor.save(os.path.join(data_path,'match_predictor.h5'))
