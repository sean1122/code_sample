from __future__ import print_function
from __future__ import division

import os
import random

import pandas as pd
import numpy as np
from tqdm import tqdm

debug = True
data_path = os.path.abspath('./Data')

# define columns of interest
columns = ['Total First Downs',
        'Number of Third Down Attempted',
        'Number of Third Down Success',
        'Number of Fourth Down Attempted',
        'Number of Fourth Down Success',
        'Total Net Yards',
        'Net Yards Rushing',
        'Net Yard Passing',
        'Number of Attempted Passes', 
        'Number of Completed Passes',
        'Number of Intercepted Passes',
        'Number of Punts',
        'Net Punting Average',
        'Number of Field Goals Blocked',
        'Number of Points After Touchdown Blocked',
        'Total Return Yardage',
        'Number of Penalties',
        'Yards backward due to Penalties',
        'Number of Fumbles',
        'Number of Lost Fumbles',
        'Number of Touchdowns',
        'Number of Extra Points Made', 
        'Number of Extra Points Attempted',
        'Number of Field Goals Made',
        'Number of Field Goals Attempted',
        'Number of Red Zone Success',
        'Number of Red Zone Attempts',
        'Number of Goal To Go Success',
        'Number of Goal To Go Attempts',
        'Number of Safeties',
        'Possession Time']

def get_matches(test=False):
    # grab all 'reg' season matches from Data
    if test:
        matches = [match for match in os.listdir(os.path.join(data_path, "Matches")) \
                    if 'REG' in match and '2001' not in match]
    else:
        matches = [match for match in os.listdir(os.path.join(data_path, "Matches")) \
                    if 'REG' in match and '2017' not in match\
                  and '2001' not in match]
    # Houston Texans was formed in 2002, and became the 32th team in NFL.

    return matches

def get_yrs_wks(matches):
    yrs = []
    wks = []
    for match in matches:
        yr, wk = match.split('_')[0].split("REG")
        yr = int(yr)
        wk = int(wk)

        if yr not in yrs:
            yrs.append(yr)

        if wk not in wks:
            wks.append(wk)

    return yrs, wks

def get_team_data(team, columns, test=False):

    matches = get_matches(test)
    yrs, wks = get_yrs_wks(matches)

    team_data = []
    for yr in sorted(yrs):
        for wk in sorted(wks):

            match_wk = str(yr)+'REG'+str(wk)+'_'

            for match in [match for match in matches if match_wk in match and team in match]:
                team_path = [match for match in os.listdir(\
                        os.path.join(data_path, 'Teams', team)) if match_wk in match][0]

                with open(os.path.join(data_path, 'Teams', team, team_path)) as f:
                    team_datum = pd.read_csv(f)
                    team_datum = team_datum[columns].as_matrix().flatten().tolist()

                team_data.append(team_datum)

    return team_data

def extract_scores(team, match_wk):
    team_label_path = [match for match in os.listdir(\
                       os.path.join(data_path, 'Teams', team)) if match_wk in match][0]

    with open(os.path.join(data_path, 'Teams', team, team_label_path), 'rb') as f:
        team_score = pd.read_csv(f)
        team_score = team_score['Final Score'].as_matrix().flatten()

    return team_score

def extract_features(team, matches_to_consider, team_networks):
    team_feature_paths = [match for match in os.listdir(os.path.join(data_path,'Teams',team))\
                          if any(past_match in match for past_match in matches_to_consider)]

    team_data = []
    for team_feature_path in team_feature_paths:
        with open(os.path.join(data_path, 'Teams', team, team_feature_path), 'rb') as f:
            team_datum = pd.read_csv(f)
            team_datum = team_datum[columns].as_matrix().flatten()
            team_datum = np.append(team_datum, np.zeros(1), axis=0)
            team_datum = np.reshape(team_datum, (team_datum.shape[0], 1))
            team_data.append(team_datum)

    team_data = np.array(team_data)
    team_data_decay = (np.arange(len(team_data))+1) / np.sum(np.arange(len(team_data))+1)

    encoded_team_datum = np.average(team_networks[team].encoder.predict(team_data,
                            batch_size=1), axis=0, weights=team_data_decay).flatten()

    return encoded_team_datum

def get_match_data(team_networks, columns, test=False):
    
    matches = get_matches(test)
    
    yrs, wks = get_yrs_wks(matches)

    match_wks = []
    for yr in sorted(yrs):
        for wk in sorted(wks):
            match_wks.append(str(yr)+'REG'+str(wk)+'_')
  
    match_data = []
    match_scores = []
    for i in range(5, len(match_wks)):
        matches_to_consider = match_wks[i-5:i]
        match_wk = match_wks[i]
        
        if test:
            if '2017' not in match_wk:
                continue

        for match in [match for match in matches if match_wk in match]:

            away, home = match.split('_')[1].split('.')[0].split('@')

            # grab the scores (groundtruths)
            away_score = extract_scores(away, match_wk)
            home_score = extract_scores(home, match_wk)

            match_score = np.append(away_score, home_score)
            match_scores.append(match_score)

            encoded_away_datum = extract_features(away, matches_to_consider, team_networks)
            encoded_home_datum = extract_features(away, matches_to_consider, team_networks)

            match_datum = np.append(encoded_away_datum, encoded_home_datum, axis=0)
            match_data.append(match_datum)
                
    return np.array(match_data), np.array(match_scores)

def categorize_labels(match_scores, mode=5):
    retval = []
    for i in range(len(match_scores)):
        if mode==5:
            if match_scores[i,0] - match_scores[i,1] <= -8.0:
                retval.append(0)
            elif match_scores[i,0] - match_scores[i,1] <= -4.0:
                retval.append(1)
            elif match_scores[i,0] - match_scores[i,1] < 4.0:
                retval.append(2)
            elif match_scores[i,0] - match_scores[i,1] < 8.0:
                retval.append(3)
            else:
                retval.append(4)

        elif mode==3:
            if match_scores[i,0] - match_scores[i,1] <= -4.0:
                retval.append(0)
            elif match_scores[i,0] - match_scores[i,1] < 4.0:
                retval.append(1)
            else:
                retval.append(2)

        elif mode==2:
            if match_scores[i,0] < match_scores[i,1]:
                retval.append(0)
            else:
                retval.append(1)

    return np.array(retval, dtype='int32')

def balance_train_data(x_train, y_train):
    train_data = np.append(x_train, np.expand_dims(y_train, axis=-1), axis=1)
    unq, unq_idx = np.unique(train_data[:,-1], return_inverse=True)
    unq_count = np.bincount(unq_idx)
    count2match = np.max(unq_count)
    balanced_train_data = np.empty((count2match*len(unq),)+train_data.shape[1:],
                                  train_data.dtype)
    for i in range(len(unq)):
        idx = np.random.choice(np.where(unq_idx==i)[0], count2match)
        balanced_train_data[i*count2match:(i+1)*count2match] = train_data[idx]
    
    return balanced_train_data[:,:-1], balanced_train_data[:,-1].astype('int32')
