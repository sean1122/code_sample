import numpy as np
import keras
from keras.layers import Input, Dense, Dropout, Conv1D, MaxPooling1D, UpSampling1D
from keras.models import Model, Sequential
import matplotlib.pyplot as plt

class MatchModel(object):

    def __init__(self, input_shape, params, output_shape):
        self.inputs = Input(shape=input_shape)

        x = Dense(params['latent_dim'], activation=params['activation'])(self.inputs)
        x = Dropout(params['dropout'])(x)
        x = Dense(params['latent_dim'], activation=params['activation'])(x)
        self.predictions = Dense(output_shape, activation='softmax')(x)

        self.model = Model(inputs=self.inputs, outputs=self.predictions)
        self.model.compile(optimizer=params['optimizer'],
                           loss=params['loss'],
                           metrics=['categorical_accuracy'])

    def train(self, x_train, y_train, params, x_valid, y_valid):
        self.hist = self.model.fit(x_train,
                                   y_train,
                                   epochs=params['n_epoch'],
                                   batch_size=params['batch_size'],
                                   validation_data=(x_valid, y_valid),
                                   verbose=params['verbose'])
        return self.hist

    def plot_train(self):
        plt.plot(self.hist.history['loss'])
        plt.plot(self.hist.history['val_loss'])
        plt.title('match predictor')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'val'], loc='upper left')
        plt.show()

    def eval(self, x_train, y_train, x_valid, y_valid, x_test, y_test):
        train_score = self.model.evaluate(x_train, y_train, batch_size=1, verbose=False)
        self.model.reset_states()
        print('Train Score:', train_score)

        valid_score = self.model.evaluate(x_valid, y_valid, batch_size=1, verbose=False)
        self.model.reset_states()
        print('Validation Score:', valid_score)

        test_score = self.model.evaluate(x_test, y_test, batch_size=1, verbose=False)
        self.model.reset_states()                                                              
        print('Test Score:', test_score)

    def predict(self, x_test, y_test):
        total_length = len(np.argmax(self.model.predict(x_test), axis=1) == \
                                    np.argmax(y_test, axis=1))

        correct_predictions = np.sum(np.argmax(self.model.predict(x_test), axis=1) == \
                                    np.argmax(y_test, axis=1))

        return correct_predictions/total_length

    def save(self, filepath):
        self.model.save(filepath)
