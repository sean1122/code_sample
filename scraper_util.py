
def post_process_nfl_match_data(away_stats, home_stats):
    '''
    This function separates the scraped stat tables, which are beautifulSoup tags,
    convert the values to float, and save them in dict format
    '''
    away_team_match_stats = {}

    away_first_downs = away_stats["Match"]["Total First Downs"]
    away_team_match_stats["Total First Downs"] = float(away_first_downs[0])
    away_team_match_stats["Number of First Down by Rushing"] = \
        float(away_first_downs[1]["By Rushing"]) if away_first_downs[1]["By Rushing"] else 0.0
    away_team_match_stats["Number of First Downs by Passing"] = float(away_first_downs[2]["By Passing"])
    away_team_match_stats["Number of First Downs by Penalty"] = \
        float(away_first_downs[3]["By Penalty"]) if away_first_downs[3]["By Penalty"] else 0.0

    away_third_down = away_stats["Match"]["Third Down Efficiency"][0].split(" - ")[0].split('/')
    away_team_match_stats["Number of Third Down Success"] = float(away_third_down[0])
    away_team_match_stats["Number of Third Down Attempted"] = float(away_third_down[1])

    away_fourth_down = away_stats["Match"]["Fourth Down Efficiency"][0].split(" - ")[0].split('/')
    away_team_match_stats["Number of Fourth Down Success"] = float(away_fourth_down[0])
    away_team_match_stats["Number of Fourth Down Attempted"] = float(away_fourth_down[1])

    away_total_net_yards = away_stats["Match"]["Total Net Yards"]
    away_team_match_stats["Total Net Yards"] = float(away_total_net_yards[0])
    away_team_match_stats["Total Rushing/Passing Plays (includes Sacks)"] = \
        float(away_total_net_yards[1]["Total Rushing/Passing Plays (includes Sacks)"])
    away_team_match_stats["Average Gain per Offensive Play"] = \
        float(away_total_net_yards[2]["Average Gain per Offensive Play"])

    away_rushing = away_stats["Match"]['Net Yards Rushing']
    away_team_match_stats["Net Yards Rushing"] = float(away_rushing[0])
    away_team_match_stats["Number of Rushing Plays"] = float(away_rushing[1]["Total Rushing Plays"])
    away_team_match_stats["Average Gain per Rushing Play"] = \
        float(away_rushing[2]["Average Gain per Rushing Play"])
    away_team_match_stats["Number of Tackled for a Loss"] = \
        float(away_rushing[3]["Tackled for a Loss (Number-Yards)"].split('--')[0]) \
        if "--" in away_rushing[3]["Tackled for a Loss (Number-Yards)"] else \
        float(away_rushing[3]["Tackled for a Loss (Number-Yards)"].split('-')[0])
    away_team_match_stats["Yardage of Tackled for a Loss"] = \
        float(away_rushing[3]["Tackled for a Loss (Number-Yards)"].split('--')[1]) \
        if "--" in away_rushing[3]["Tackled for a Loss (Number-Yards)"] else \
        float(away_rushing[3]["Tackled for a Loss (Number-Yards)"].split('-')[1])

    away_passing = away_stats["Match"]["Net Yards Passing"]
    away_team_match_stats["Net Yard Passing"] = float(away_passing[0])
    away_team_match_stats["Number of Times Sacked"] = \
        float(away_passing[1]["Times Sacked (Number-Yards)"].split(" - ")[0])
    away_team_match_stats["Yardages When Sacked"] = \
        float(away_passing[1]["Times Sacked (Number-Yards)"].split(" - ")[1])
    away_team_match_stats["Gross Yards Passing"] = \
        float(away_passing[2]["Gross Yards Passing"])

    away_pass_stats = away_stats["Match"]["Pass Comp-Att-Int"]
    away_team_match_stats["Number of Completed Passes"] = float(away_pass_stats[0].split(" - ")[0])
    away_team_match_stats["Number of Attempted Passes"] = float(away_pass_stats[0].split(" - ")[1])
    away_team_match_stats["Number of Intercepted Passes"] = float(away_pass_stats[0].split(" - ")[2])
    away_team_match_stats["Average Gain per Pass"] = \
        float(away_pass_stats[1]["Average Gain per Passing Play (includes\tSacks)"])

    away_kickoffs = away_stats["Match"]["Kickoffs (Number-In End Zone-Touchbacks)"][0].split(" - ")
    away_team_match_stats["Number of Kickoffs"] = float(away_kickoffs[0])
    away_team_match_stats["Number of Kickoffs in End Zone"] = float(away_kickoffs[1])
    away_team_match_stats["Number of Kickoff Touchbacks"] = float(away_kickoffs[2])

    away_punts = away_stats["Match"]["Punts (Number-Average)"]
    away_team_match_stats["Number of Punts"] = \
        float(away_punts[0].split('-')[0]) if away_punts[0].split('-')[0] is not '' \
        else 0.0
    away_team_match_stats["Average Punt Yardage"] = \
        float(away_punts[0].split('-')[1]) if away_punts[0].split('-')[1] is not '' \
        else 0.0
    away_team_match_stats["Number of Punts Blocked"] = float(away_punts[1]["Blocked"])
    away_team_match_stats["Net Punting Average"] = \
        float(away_punts[2]["Net Punting Average"]) if away_punts[2]["Net Punting Average"] is not '' \
        else 0.0

    away_fgs_pats_blocked = away_stats["Match"]["FGs Blocked - PATs Blocked"][0].split(" - ")
    away_team_match_stats["Number of Field Goals Blocked"] = float(away_fgs_pats_blocked[0])
    away_team_match_stats["Number of Points After Touchdown Blocked"] = float(away_fgs_pats_blocked[1])

    away_return_yardage = away_stats["Match"]['Total Return Yardage (excludes Kickoffs)']
    away_team_match_stats["Total Return Yardage"] = float(away_return_yardage[0])
    away_team_match_stats["Number of Punt Regurns"] = \
        float(away_return_yardage[1]["Punt Returns (Number-Yards)"].split(" - ")[0])
    away_team_match_stats["Punt Regurns Yardage"] = \
        float(away_return_yardage[1]["Punt Returns (Number-Yards)"].split(" - ")[1])
    away_team_match_stats["Number of Kickoff Returns"] = \
        float(away_return_yardage[2]["Kickoff Returns (Number-Yards)"].split(" - ")[0])
    away_team_match_stats["Kickoff Returns Yardage"] = \
        float(away_return_yardage[2]["Kickoff Returns (Number-Yards)"].split(" - ")[1])
    away_team_match_stats["Number of Interception Returns"] = \
        float(away_return_yardage[3]["Interception Returns (Number-Yards)"].split(" - ")[0])
    away_team_match_stats["Interception Returns Yardage"] = \
        float(away_return_yardage[3]["Interception Returns (Number-Yards)"].split(" - ")[1])

    away_penalties = away_stats["Match"]["Penalties (Number-Yards)"][0].split(" - ")
    away_team_match_stats["Number of Penalties"] = float(away_penalties[0])
    away_team_match_stats["Yards backward due to Penalties"] = float(away_penalties[-1])

    away_fumbles = away_stats["Match"]["Fumbles (Number-Lost)"][0].split(" - ")
    away_team_match_stats["Number of Fumbles"] = float(away_fumbles[0])
    away_team_match_stats["Number of Lost Fumbles"] = float(away_fumbles[-1])

    away_touchdowns = away_stats["Match"]["Touchdowns"]
    away_team_match_stats["Number of Touchdowns"] = float(away_touchdowns[0])
    away_team_match_stats["Number of Touchdowns by Rushing"] = float(away_touchdowns[1]["Rushing"])
    away_team_match_stats["Number of Touchdowns by Passing"] = float(away_touchdowns[2]["Passing"])
    away_team_match_stats["Number of Touchdowns by Interceptions"] = \
        float(away_touchdowns[3]["Interceptions"])
    away_team_match_stats["Number of Touchdowns by Kickoff Returns"] = \
        float(away_touchdowns[4]["Kickoff Returns"])
    away_team_match_stats["Number of Touchdowns by Fumble Returns"] = \
        float(away_touchdowns[5]["Fumble Returns"])
    away_team_match_stats["Number of Touchdowns by Punt Returns"] = \
        float(away_touchdowns[6]["Punt Returns"])

    away_extra_points = away_stats["Match"]['Extra Points (Made-Attempted)']
    away_team_match_stats["Number of Extra Points Made"] = float(away_extra_points[0].split(" - ")[0])
    away_team_match_stats["Number of Extra Points Attempted"] = \
        float(away_extra_points[0].split(" - ")[1])
    away_team_match_stats["Number of Extra Points by Kicking Made"] = \
        float(away_extra_points[1]["Kicking (Made-Attempted)"].split(" - ")[0])
    away_team_match_stats["Number of Extra Points by Kicking Attempted"] = \
        float(away_extra_points[1]["Kicking (Made-Attempted)"].split(" - ")[1])
    away_team_match_stats["Number of Extra Points 2 Pt Conversions Made"] =\
        float(away_extra_points[2]["Two Point Conversions (Made-Attempted)"].split(" - ")[0])
    away_team_match_stats["Number of Extra Points 2 Pt Conversions Attempted"] =\
        float(away_extra_points[2]["Two Point Conversions (Made-Attempted)"].split(" - ")[1])

    away_field_goals = away_stats["Match"]["Field Goals (Made-Attempted)"][0].split(" - ")
    away_team_match_stats["Number of Field Goals Made"] = float(away_field_goals[0])
    away_team_match_stats["Number of Field Goals Attempted"] = float(away_field_goals[-1])

    away_red_zone = away_stats["Match"]["Red Zone Efficiency"][0].split(" - ")[0].split('/')
    away_team_match_stats["Number of Red Zone Success"] = float(away_red_zone[0])
    away_team_match_stats["Number of Red Zone Attempts"] = float(away_red_zone[1])

    away_goal_to_go = away_stats["Match"]["Goal To Go Efficiency"][0].split(" - ")[0].split('/')
    away_team_match_stats["Number of Goal To Go Success"] = float(away_goal_to_go[0])
    away_team_match_stats["Number of Goal To Go Attempts"] = float(away_goal_to_go[1])

    away_safeties = away_stats["Match"]["Safeties"][0]
    away_team_match_stats["Number of Safeties"] = float(away_safeties)

    away_possession_time = away_stats["Match"]["Time of Possession"][0].split(':')
    away_team_match_stats["Possession Time"] = \
        float(away_possession_time[0])*60 + float(away_possession_time[-1])

    away_team_match_stats["Home/Away (0/1)"] = int(1)

    away_team_match_stats["Final Score"] = float(away_stats["Match"]["Final Score"][0])



    home_team_match_stats = {}

    home_first_downs = home_stats["Match"]["Total First Downs"]
    home_team_match_stats["Total First Downs"] = float(home_first_downs[0])
    home_team_match_stats["Number of First Down by Rushing"] = \
        float(home_first_downs[1]["By Rushing"]) if home_first_downs[1]["By Rushing"] else 0.0
    home_team_match_stats["Number of First Downs by Passing"] = float(home_first_downs[2]["By Passing"])
    home_team_match_stats["Number of First Downs by Penalty"] = \
        float(home_first_downs[3]["By Penalty"]) if home_first_downs[3]["By Penalty"] else 0.0

    home_third_down = home_stats["Match"]["Third Down Efficiency"][0].split(" - ")[0].split('/')
    home_team_match_stats["Number of Third Down Success"] = float(home_third_down[0])
    home_team_match_stats["Number of Third Down Attempted"] = float(home_third_down[1])

    home_fourth_down = home_stats["Match"]["Fourth Down Efficiency"][0].split(" - ")[0].split('/')
    home_team_match_stats["Number of Fourth Down Success"] = float(home_fourth_down[0])
    home_team_match_stats["Number of Fourth Down Attempted"] = float(home_fourth_down[1])

    home_total_net_yards = home_stats["Match"]["Total Net Yards"]
    home_team_match_stats["Total Net Yards"] = float(home_total_net_yards[0])
    home_team_match_stats["Total Rushing/Passing Plays (includes Sacks)"] = \
        float(home_total_net_yards[1]["Total Rushing/Passing Plays (includes Sacks)"])
    home_team_match_stats["Average Gain per Offensive Play"] = \
        float(home_total_net_yards[2]["Average Gain per Offensive Play"])

    home_rushing = home_stats["Match"]['Net Yards Rushing']
    home_team_match_stats["Net Yards Rushing"] = float(home_rushing[0])
    home_team_match_stats["Number of Rushing Plays"] = float(home_rushing[1]["Total Rushing Plays"])
    home_team_match_stats["Average Gain per Rushing Play"] = \
        float(home_rushing[2]["Average Gain per Rushing Play"])
    home_team_match_stats["Number of Tackled for a Loss"] = \
        float(home_rushing[3]["Tackled for a Loss (Number-Yards)"].split('--')[0]) \
        if "--" in home_rushing[3]["Tackled for a Loss (Number-Yards)"] else \
        float(home_rushing[3]["Tackled for a Loss (Number-Yards)"].split('-')[0])
    home_team_match_stats["Yardage of Tackled for a Loss"] = \
        float(home_rushing[3]["Tackled for a Loss (Number-Yards)"].split('--')[1]) \
        if "--" in home_rushing[3]["Tackled for a Loss (Number-Yards)"] else \
        float(home_rushing[3]["Tackled for a Loss (Number-Yards)"].split('-')[1])

    home_passing = home_stats["Match"]["Net Yards Passing"]
    home_team_match_stats["Net Yard Passing"] = float(home_passing[0])
    home_team_match_stats["Number of Times Sacked"] = \
        float(home_passing[1]["Times Sacked (Number-Yards)"].split(" - ")[0])
    home_team_match_stats["Yardages When Sacked"] = \
        float(home_passing[1]["Times Sacked (Number-Yards)"].split(" - ")[1])
    home_team_match_stats["Gross Yards Passing"] = \
        float(home_passing[2]["Gross Yards Passing"])

    home_pass_stats = home_stats["Match"]["Pass Comp-Att-Int"]
    home_team_match_stats["Number of Completed Passes"] = float(home_pass_stats[0].split(" - ")[0])
    home_team_match_stats["Number of Attempted Passes"] = float(home_pass_stats[0].split(" - ")[1])
    home_team_match_stats["Number of Intercepted Passes"] = float(home_pass_stats[0].split(" - ")[2])
    home_team_match_stats["Average Gain per Pass"] = \
        float(home_pass_stats[1]["Average Gain per Passing Play (includes\tSacks)"])

    home_kickoffs = home_stats["Match"]["Kickoffs (Number-In End Zone-Touchbacks)"][0].split(" - ")
    home_team_match_stats["Number of Kickoffs"] = float(home_kickoffs[0])
    home_team_match_stats["Number of Kickoffs in End Zone"] = float(home_kickoffs[1])
    home_team_match_stats["Number of Kickoff Touchbacks"] = float(home_kickoffs[2])

    home_punts = home_stats["Match"]["Punts (Number-Average)"]
    home_team_match_stats["Number of Punts"] = \
        float(home_punts[0].split('-')[0]) if home_punts[0].split('-')[0] is not '' \
        else 0.0
    home_team_match_stats["Average Punt Yardage"] = \
        float(home_punts[0].split('-')[1]) if home_punts[0].split('-')[1] is not '' \
        else 0.0
    home_team_match_stats["Number of Punts Blocked"] = float(home_punts[1]["Blocked"])
    home_team_match_stats["Net Punting Average"] = \
        float(home_punts[2]["Net Punting Average"]) if home_punts[2]["Net Punting Average"] is not '' \
        else 0.0

    home_fgs_pats_blocked = home_stats["Match"]["FGs Blocked - PATs Blocked"][0].split(" - ")
    home_team_match_stats["Number of Field Goals Blocked"] = float(home_fgs_pats_blocked[0])
    home_team_match_stats["Number of Points After Touchdown Blocked"] = float(home_fgs_pats_blocked[1])

    home_return_yardage = home_stats["Match"]['Total Return Yardage (excludes Kickoffs)']
    home_team_match_stats["Total Return Yardage"] = float(home_return_yardage[0])
    home_team_match_stats["Number of Punt Regurns"] = \
        float(home_return_yardage[1]["Punt Returns (Number-Yards)"].split(" - ")[0])
    home_team_match_stats["Punt Regurns Yardage"] = \
        float(home_return_yardage[1]["Punt Returns (Number-Yards)"].split(" - ")[1])
    home_team_match_stats["Number of Kickoff Returns"] = \
        float(home_return_yardage[2]["Kickoff Returns (Number-Yards)"].split(" - ")[0])
    home_team_match_stats["Kickoff Returns Yardage"] = \
        float(home_return_yardage[2]["Kickoff Returns (Number-Yards)"].split(" - ")[1])
    home_team_match_stats["Number of Interception Returns"] = \
        float(home_return_yardage[3]["Interception Returns (Number-Yards)"].split(" - ")[0])
    home_team_match_stats["Interception Returns Yardage"] = \
        float(home_return_yardage[3]["Interception Returns (Number-Yards)"].split(" - ")[1])

    home_penalties = home_stats["Match"]["Penalties (Number-Yards)"][0].split(" - ")
    home_team_match_stats["Number of Penalties"] = float(home_penalties[0])
    home_team_match_stats["Yards backward due to Penalties"] = float(home_penalties[-1])

    home_fumbles = home_stats["Match"]["Fumbles (Number-Lost)"][0].split(" - ")
    home_team_match_stats["Number of Fumbles"] = float(home_fumbles[0])
    home_team_match_stats["Number of Lost Fumbles"] = float(home_fumbles[-1])

    home_touchdowns = home_stats["Match"]["Touchdowns"]
    home_team_match_stats["Number of Touchdowns"] = float(home_touchdowns[0])
    home_team_match_stats["Number of Touchdowns by Rushing"] = float(home_touchdowns[1]["Rushing"])
    home_team_match_stats["Number of Touchdowns by Passing"] = float(home_touchdowns[2]["Passing"])
    home_team_match_stats["Number of Touchdowns by Interceptions"] = \
        float(home_touchdowns[3]["Interceptions"])
    home_team_match_stats["Number of Touchdowns by Kickoff Returns"] = \
        float(home_touchdowns[4]["Kickoff Returns"])
    home_team_match_stats["Number of Touchdowns by Fumble Returns"] = \
        float(home_touchdowns[5]["Fumble Returns"])
    home_team_match_stats["Number of Touchdowns by Punt Returns"] = \
        float(home_touchdowns[6]["Punt Returns"])

    home_extra_points = home_stats["Match"]['Extra Points (Made-Attempted)']
    home_team_match_stats["Number of Extra Points Made"] = float(home_extra_points[0].split(" - ")[0])
    home_team_match_stats["Number of Extra Points Attempted"] = float(home_extra_points[0].split(" - ")[1])
    home_team_match_stats["Number of Extra Points by Kicking Made"] = \
        float(home_extra_points[1]["Kicking (Made-Attempted)"].split(" - ")[0])
    home_team_match_stats["Number of Extra Points by Kicking Attempted"] = \
        float(home_extra_points[1]["Kicking (Made-Attempted)"].split(" - ")[1])
    home_team_match_stats["Number of Extra Points 2 Pt Conversions Made"] =\
        float(home_extra_points[2]["Two Point Conversions (Made-Attempted)"].split(" - ")[0])
    home_team_match_stats["Number of Extra Points 2 Pt Conversions Attempted"] =\
        float(home_extra_points[2]["Two Point Conversions (Made-Attempted)"].split(" - ")[1])

    home_field_goals = home_stats["Match"]["Field Goals (Made-Attempted)"][0].split(" - ")
    home_team_match_stats["Number of Field Goals Made"] = float(home_field_goals[0])
    home_team_match_stats["Number of Field Goals Attempted"] = float(home_field_goals[-1])

    home_red_zone = home_stats["Match"]["Red Zone Efficiency"][0].split(" - ")[0].split('/')
    home_team_match_stats["Number of Red Zone Success"] = float(home_red_zone[0])
    home_team_match_stats["Number of Red Zone Attempts"] = float(home_red_zone[1])

    home_goal_to_go = home_stats["Match"]["Goal To Go Efficiency"][0].split(" - ")[0].split('/')
    home_team_match_stats["Number of Goal To Go Success"] = float(home_goal_to_go[0])
    home_team_match_stats["Number of Goal To Go Attempts"] = float(home_goal_to_go[1])

    home_safeties = home_stats["Match"]["Safeties"][0]
    home_team_match_stats["Number of Safeties"] = float(home_safeties)

    home_possession_time = home_stats["Match"]["Time of Possession"][0].split(':')
    home_team_match_stats["Possession Time"] = \
        float(home_possession_time[0])*60 + float(home_possession_time[-1])

    home_team_match_stats["Home/Away (0/1)"] = int(0)

    home_team_match_stats["Final Score"] = float(home_stats["Match"]["Final Score"][0])

    return away_team_match_stats, home_team_match_stats
