from __future__ import division
from __future__ import print_function

import json
import os
import sys
import time
import datetime
import random
import argparse

import requests
from bs4 import BeautifulSoup
import pandas as pd
from tqdm import tqdm

from scraper_util import *

random.seed(time.time())

# set up the paths
data_path = os.path.abspath('./Data')

if not os.path.exists(data_path):
    os.mkdir(data_path)

match_data_path = os.path.join(data_path, 'Matches')

if not os.path.exists(match_data_path):
    os.mkdir(match_data_path)

team_data_path = os.path.join(data_path, 'Teams')

if not os.path.exists(team_data_path):
    os.mkdir(team_data_path)


def extract_match_ids(matches):
    '''
    extract matches ids to ping each match page.
    pretty ugly
    '''
    match_ids = [(match[-1].split('@')[0].split('/')[-4], match[0], match[1]) \
                for match in matches if '@' in match[-1]]
    match_info_base_url = 'http://www.nfl.com/widget/gc/2011/tabs/cat-post-boxscore?enableNGS=false&gameId='
    match_urls = [(match_info_base_url+match_id[0], match_id[1], match_id[2]) \
                for match_id in match_ids]

    return match_urls

def build_match_queue(simplify=True):
    '''
    Scraps all matches since 2002, and put them in a queue
    '''
    matches = []

    url = 'http://www.nfl.com/schedules'
    schedules_page = requests.get(url)

    now = datetime.datetime.now()
    if not simplify:
        # a new team is formed in 2002
        yrs = [str(yr) for yr in xrange(2002,now.year+1)]
    else:
        yrs = [str(now.year)]

    for yr in tqdm(yrs):
        season_schedule_page = requests.get('/'.join([schedules_page.url, yr, 'REG1']))
        soup = BeautifulSoup(season_schedule_page.content, 'lxml')

        _, _, _, wks = soup.find_all(class_='page-nav-template')
        wks = BeautifulSoup(wks.contents[0], 'lxml').find_all('a')
        wks = [wk.get_text(strip=True) for wk in wks]

        for wk in wks:
            if not unicode.isnumeric(wk):
                wk = '0'

            matches_page = requests.get(schedules_page.url+'/'+yr+'/REG'+wk)

            soup = BeautifulSoup(matches_page.content, 'lxml')
            match_links = [match['href'] for match in soup.find_all('a', class_='gc-btn')]

            for match_link in match_links:
                matches.append((yr, wk, match_link))

    match_q = extract_match_ids(matches)

    return match_q

def scrape(match_q):
    failed_items = []

    for match_item in tqdm(match_q):
        url = match_item[0]
        yr = match_item[1]
        wk = match_item[2]

        try:
            match_page = requests.get(url)

            # inject artificial random delay so I don't get blocked
            time.sleep(random.randint(250,750) / 1000)

            soup = BeautifulSoup(match_page.content, 'lxml')
            away_team, home_team = [team.get_text(strip=True).split()[-1] \
                                    for team in soup.select('.team-column-header a')]
            away_stats = {}
            home_stats = {}
            away_stats['Team'] = away_team
            home_stats['Team'] = home_team

            match_team_stats = soup.find_all(class_='gc-box-score-table')[-1]

            away_stats['Match'] = {}
            home_stats['Match'] = {}

            # get all away team stats
            cur_category = None
            for row in match_team_stats.find_all('tr'):
                data = [cell.get_text(strip=True).replace(u'\xa0',' ') for cell in row.find_all('td')][:2]
                if row['class'][0] == 'thd2':
                    cur_category = data[0]
                    away_stats['Match'][cur_category] = [data[1]]
                else:
                    away_stats['Match'][cur_category].append({data[0]: data[1]})

            # get all home team stats
            cur_category = None
            for row in match_team_stats.find_all('tr'):
                data = [cell.get_text(strip=True).replace(u'\xa0',' ') for cell in row.find_all('td')][-2:]
                if row['class'][0] == 'thd2':
                    cur_category = data[0]
                    home_stats['Match'][cur_category] = [data[1]]
                else:
                    home_stats['Match'][cur_category].append({data[0]: data[1]})
            
            away_team_match_stats, home_team_match_stats = post_process_nfl_match_data(away_stats, home_stats)
            away_stats['Match'] = pd.DataFrame(away_team_match_stats, index=[0])
            home_stats['Match'] = pd.DataFrame(home_team_match_stats, index=[0])

            away_data_path = os.path.join(team_data_path, away_stats['Team'])
            home_data_path = os.path.join(team_data_path, home_stats['Team'])

            if not os.path.exists(away_data_path):
                os.mkdir(away_data_path)
            if not os.path.exists(home_data_path):
                os.mkdir(home_data_path)

            cur_match_data_path = os.path.join(match_data_path, \
                    ''.join([yr,'REG',wk,'_',away_stats['Team'],'@',home_stats['Team'],'.csv']))
            pd.concat([away_stats['Match'], home_stats['Match']], axis=1).to_csv(cur_match_data_path)

            cur_match_away_data_path = os.path.join(away_data_path, ''.join([yr,'REG',wk,'_A.csv']))
            away_stats['Match'].to_csv(cur_match_away_data_path)

            cur_match_home_data_path = os.path.join(home_data_path, ''.join([yr,'REG',wk,'_H.csv']))
            home_stats['Match'].to_csv(cur_match_home_data_path)

        except Exception as e:
            print(e)
            failed_items.append((url, yr, wk))

    return failed_items

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Scrapes team data')
    parser.add_argument('--hist', dest='historic', action='store_false')
    args = parser.parse_args()

    simplify = args.historic

    # bulid the match queue
    match_q = build_match_queue(simplify=simplify)

    count = 0
    while len(match_q) > 0:
        print(count,'th trial')
        match_q = scrape(match_q)
        # inject artificial random delay so I don't get blocked
        time.sleep(random.randint(5,10))

        if count == 3:
            print('something went horrible!')
            break

    print('Finished Scraping!')
