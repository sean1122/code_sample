from __future__ import print_function

import numpy as np
import keras
from keras.layers import Input, Dense, Dropout, Conv1D, MaxPooling1D, UpSampling1D
from keras.models import Model, Sequential
import matplotlib.pyplot as plt


class TeamModel(object):

    def __init__(self, team, input_shape, params):

        self.team = team

        self.inputs = Input(shape=input_shape)

        # encoding layer
        self.encoded = Conv1D(params['latent_dim'],
                              params['timesteps'],
                              activation=params['activation'],
                              padding='same')(self.inputs)
        self.encoded = MaxPooling1D(pool_size=2, padding='same')(self.encoded)

        # decoding layer
        self.decoded = Conv1D(params['latent_dim'],
                              params['timesteps'],
                              activation=params['activation'],
                              padding='same')(self.encoded)
        self.decoded = UpSampling1D(size=2)(self.decoded)
        self.decoded = Conv1D(1,
                              params['timesteps'],
                              activation=params['activation'],
                              padding='same')(self.decoded)

        self.autoencoder = Model(self.inputs, self.decoded)
        self.encoder = Model(self.inputs, self.encoded)

        self.autoencoder.compile(optimizer=params['optimizer'],
                                 loss=params['loss'])

    def train(self, x_train, y_train, params, x_valid, y_valid):

        self.hist = self.autoencoder.fit(x_train,
                                         y_train,
                                         epochs=params['n_epoch'],
                                         batch_size=params['batch_size'],
                                         validation_data=(x_valid, y_valid),
                                         verbose=params['verbose'])

        return self.hist

    def plot_train(self):
        plt.plot(self.hist.history['loss'])
        plt.plot(self.hist.history['val_loss'])
        plt.title('{}'.format(self.team))
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'val'], loc='upper left')
        plt.show()

    def eval(self, x_train, y_train, x_valid, y_valid, x_test, y_test):
        train_score = self.autoencoder.evaluate(x_train, y_train, batch_size=1, verbose=False)
        self.autoencoder.reset_states()
        print('Train Score:', train_score)

        valid_score = self.autoencoder.evaluate(x_valid, y_valid, batch_size=1, verbose=False)
        self.autoencoder.reset_states()
        print('Validation Score:', valid_score)
        
        test_score = self.autoencoder.evaluate(x_test, y_test, batch_size=1, verbose=False)
        self.autoencoder.reset_states()
        print('Test Score:', test_score)

    def predict(self, x_valid):
        decoded_tmp = self.autoencoder.predict(x_valid, batch_size=1)

        std = np.std(x_valid, axis=0).flatten()
        diff = np.mean(decoded_tmp - x_valid, axis=0).flatten()
        print('average reconstructed - groundtruth:', diff)
        print('difference in stds:',
                np.nan_to_num(np.divide(diff,std)).flatten())

    def save(self, filepath):
        self.encoder.save(filepath)
