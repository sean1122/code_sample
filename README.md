# code_sample

NFL Match Predictor.
It trains a team summary network for each team given past game statistics.
These summaries are fed to the match predictor, another deep neural network, to predict which of the two teams would win in the upcoming match.
run end_to_end_run.py to build the predictor.
The trained network is saved as "match_predictor.h5" in Data folder, which can be loaded for inference.